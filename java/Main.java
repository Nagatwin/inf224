import java.io.IOException;

public class Main {
	static final String DEFAULT_HOST = "localhost";
	static final int DEFAULT_PORT = 3331;

	public static void main(String[] args) {
		try {
			// Default window
			new MainWindow(DEFAULT_HOST, DEFAULT_PORT);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
