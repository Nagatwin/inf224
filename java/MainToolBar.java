import javax.swing.JButton;
import javax.swing.JToolBar;

/**
 * @author Guillaume Class for the toolbar of the main window
 */
public class MainToolBar extends JToolBar {
	private static final long serialVersionUID = 1L;

	public MainToolBar(MainWindow mainWindow) {
		super();

		// For issues on Macs
		System.setProperty("apple.laf.useScreenMenuBar", "true");

		// Add buttons
		this.add(new JButton(new ActionSearch(mainWindow)));
		this.add(new JButton(new ActionPlay(mainWindow)));
		this.add(new JButton(new ActionExit(mainWindow)));
	}
}
