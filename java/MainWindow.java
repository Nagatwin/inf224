import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * @author Guillaume Class for exit actions
 */
class ActionExit extends AbstractAction {
	private static final long serialVersionUID = 1L;

	MainWindow mainWindow;

	public ActionExit(MainWindow mainWindow) {
		super("Exit");
		this.mainWindow = mainWindow;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// On activation we send a request to exit
		mainWindow.exit();
	}
}

/**
 * @author Guillaume Class for search actions
 */
class ActionSearch extends AbstractAction {
	private static final long serialVersionUID = 1L;
	MainWindow mainWindow;

	public ActionSearch(MainWindow mainWindow) {
		super("Search");
		this.mainWindow = mainWindow;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// On activation we ask the search string with a dialog
		// then we search for it
		mainWindow.search(JOptionPane.showInputDialog("Search item :"));
	}
}

/**
 * @author Guillaume Class for play actions
 */
class ActionPlay extends AbstractAction {
	private static final long serialVersionUID = 1L;
	MainWindow mainWindow;

	public ActionPlay(MainWindow mainWindow) {
		super("Play");
		this.mainWindow = mainWindow;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// On activation we ask the play item with a dialog
		// then we play it
		mainWindow.play(JOptionPane.showInputDialog("Play item :"));
	}
}

/**
 * @author Guillaume Main window class
 */
public class MainWindow extends JFrame {
	private static final long serialVersionUID = 1L;

	// Socket variables
	private Socket sock;
	private BufferedReader input;
	private BufferedWriter output;

	// Text area for logs
	private final JTextArea logs;

	/**
	 * @param host
	 *            host to connect to
	 * @param port
	 *            port to connect to
	 * @throws UnknownHostException
	 * @throws IOException
	 */
	public MainWindow(String host, int port) throws UnknownHostException, IOException {
		super();

		// We try to connect to the server
		try {
			sock = new java.net.Socket(host, port);
		} catch (java.net.UnknownHostException e) {
			System.err.println("Client: Couldn't find host " + host + ":" + port);
			throw e;
		} catch (java.io.IOException e) {
			System.err.println("Client: Couldn't reach host " + host + ":" + port);
			throw e;
		}

		try {
			input = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			output = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
		} catch (java.io.IOException e) {
			System.err.println("Client: Couldn't open input or output streams");
			throw e;
		}

		// We build the window's components, see concerned classes
		this.setTitle("Client");

		this.setJMenuBar(new MainMenuBar(this));

		this.add(new MainToolBar(this), BorderLayout.PAGE_START);

		// Scroll pane for the text area
		this.add(new JScrollPane(this.logs = new JTextArea(20, 100)), BorderLayout.CENTER);
		this.add(new ButtonPanel(this), BorderLayout.SOUTH);

		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		this.pack();
		this.setVisible(true);
	}

	/**
	 * Search an item on the server
	 * 
	 * @param searchString
	 *            Item name to search for
	 */
	public void search(String searchString) {
		// Send
		this.log("Sending searching request to server \"" + searchString + "\"");
		String response = this.send("search#" + searchString);

		// Check response
		if (response.substring(0, 2).equals("OK")) {
			// Parse arguments
			this.log("Response : " + response.substring(response.lastIndexOf('#') + 1));
		} else {
			// Failed
			this.log("Request failed");
			this.log(response);
		}
	}

	/**
	 * Play an item on the server
	 * 
	 * @param playItem
	 *            Item name to play
	 */
	public void play(String playItem) {
		// Send
		this.log("Sending playing request to server \"" + playItem + "\"");
		String response = this.send("play#" + playItem);

		// Check response
		if (response.substring(0, 2).equals("OK")) {
			this.log("Played successfully");
		} else {
			// Failed
			this.log("Request failed");
			this.log(response);
		}
	}

	/**
	 * Send a request to the server and get the response
	 * 
	 * @param request
	 *            request to send
	 * @return response from the server
	 */
	public String send(String request) {
		// Envoyer la requete au serveur
		try {
			request += "\n"; // ajouter le separateur de lignes
			output.write(request, 0, request.length());
			output.flush();
		} catch (java.io.IOException e) {
			System.err.println("Client: Couldn't send message: " + e);
			return null;
		}

		// Recuperer le resultat envoye par le serveur
		try {
			return input.readLine();
		} catch (java.io.IOException e) {
			System.err.println("Client: Couldn't receive message: " + e);
			return null;
		}
	}

	/**
	 * Exits the app
	 */
	public void exit() {
		System.exit(NORMAL);
	}

	/**
	 * Add a line to the text area
	 * 
	 * @param string
	 *            Line to add
	 */
	public void log(String string) {
		logs.setText(logs.getText() + string + System.lineSeparator());
	}
}
