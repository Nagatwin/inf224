import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * @author Guillaume Class for the buttons panel
 */
public class ButtonPanel extends JPanel {
	private static final long serialVersionUID = 1L;

	public ButtonPanel(MainWindow mainWindow) {
		super();

		// Add buttons
		this.add(new JButton(new ActionSearch(mainWindow)));
		this.add(new JButton(new ActionPlay(mainWindow)));
		this.add(new JButton(new ActionExit(mainWindow)));
	}
}
