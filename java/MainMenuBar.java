import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 * @author Guillaume Class for the menu bar on the main window
 */
public class MainMenuBar extends JMenuBar {
	private static final long serialVersionUID = 1L;

	private JMenu menu;

	public MainMenuBar(MainWindow mainWindow) {
		super();

		this.add(this.menu = new JMenu("Menu"));

		// Add menu items
		menu.add(new JMenuItem(new ActionSearch(mainWindow)));
		menu.add(new JMenuItem(new ActionPlay(mainWindow)));
		menu.add(new JMenuItem(new ActionExit(mainWindow)));
	}

}
