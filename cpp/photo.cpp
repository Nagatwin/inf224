﻿#include "photo.h"

void Photo::write(std::ostream &f) const {
  // Write parent attributes
  Multimedia::write(f);

  // Write attributes
  f << latitude << '\n' << longitude << '\n';
}

void Photo::read(std::istream &f) {
  // Read parent attributes
  Multimedia::read(f);

  // Read attributes
  std::string s;
  std::getline(f, s);
  latitude = stof(s);
  std::getline(f, s);
  longitude = stof(s);
}

void Photo::display(std::ostream &_s) const {
  _s << "Photo : Latitude : " << latitude << " || Longitude : " << longitude
     << " >> herited ";
  Multimedia::display(_s);
}
