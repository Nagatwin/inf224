﻿#include "film.h"

// Make independant copies each time
Film::Film(std::string _name, std::string _path, int *_chaptersDuration,
           int _chapterCount)
    : Video(_name, _path, 0) {
  setChapters(_chaptersDuration, _chapterCount);
}

Film::Film(const Film &from) : Video(from.getName(), from.getPath(), 0) {
  setChapters(from.getChaptersDuration(), from.getChapterCount());
}

Film &Film::operator=(const Film &from) {
  Video::operator=(from);

  setChapters(from.getChaptersDuration(), from.getChapterCount());
  return *this;
}

void Film::display(std::ostream &_s) const {
  // Display attributes
  _s << "Film : ChapterCount : " << chapterCount;
  for (int i = 0; i < chapterCount; i++) {
    _s << " || Chapter " << i << " : " << chaptersDuration[i];
  }

  // Display parent attributes
  _s << " >> herited ";
  Video::display(_s);
}

void Film::setChapters(int const *_chaptersDuration, int _chapterCount) {
  // Delete previous table, if any
  if (chaptersDuration != nullptr) {
    delete[] chaptersDuration;
    chaptersDuration = nullptr;
  }

  int totalDuration = 0;
  chaptersDuration = new int[_chapterCount];

  // Independant copy of durations
  for (int i = 0; i < _chapterCount; i++) {
    chaptersDuration[i] = _chaptersDuration[i];
    totalDuration += _chaptersDuration[i];
  }
  chapterCount = _chapterCount;

  // Set total duration
  setDuration(totalDuration);
}

int const *Film::getChaptersDuration() const { return chaptersDuration; }

void Film::write(std::ostream &f) const {
  // Write parent
  Video::write(f);

  // Write attributes
  f << chapterCount << '\n';
  for (int i = 0; i < chapterCount; i++) {
    f << chaptersDuration[i] << '\n';
  }
}

void Film::read(std::istream &f) {
  // Read parent
  Video::read(f);

  // Read the amount of chapters
  std::string s;
  std::getline(f, s);
  int newChapterCount = stoi(s);

  // Read chapters
  int *newChapters = new int[newChapterCount];

  for (int i = 0; i < newChapterCount; i++) {
    std::getline(f, s);
    newChapters[i] = stoi(s);
  }

  // Set chapters (no memory leak using this method)
  setChapters(newChapters, newChapterCount);

  delete newChapters;
}
