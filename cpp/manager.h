﻿#ifndef MANAGER_H
#define MANAGER_H
#include "film.h"
#include "group.h"
#include "multimedia.h"
#include "photo.h"
#include "tcpserver.h"
#include "video.h"
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>

/**
 * @brief The Manager class
 */
class Manager {
private:
  std::map<std::string, MultimediaPtr> objets;
  std::map<std::string, GroupPtr> groups;

public:
  /**
   * @brief Manager constructor
   */
  Manager() {}

  /**
   * @brief ~Manager destructor
   */
  virtual ~Manager() {
    // Random default destructor
    std::cout << "Destructeur de manager" << std::endl;
  }

  /**
   * @brief Create a new Photo
   * @param _name name of the Photo
   * @param _path path of the Photo
   * @param _latitude latitude of the Photo
   * @param _longitude longitude of the Photo
   * @return Smart pointer to the created Photo
   */
  MultimediaPtr createPhoto(std::string _name, std::string _path,
                            float _latitude, float _longitude);

  /**
   * @brief Create a new Video
   * @param _name name of the Video
   * @param _path path of the Video
   * @param _duration duration of the Video
   * @return Smart pointer to the created Video
   */
  MultimediaPtr createVideo(std::string _name, std::string _path,
                            int _duration);

  /**
   * @brief Create a new Film
   * @param _name name of the Film
   * @param _pathPath of the Film
   * @param _chaptersDuration chapters duration of the Film as an int table
   * @param _chapterCount chapters count of the Film
   * @return Smart pointer to the created Film
   */
  MultimediaPtr createFilm(std::string _name, std::string _path,
                           int *_chaptersDuration, int _chapterCount);

  /**
   * @brief Create a new Group
   * @param _name name of the Group
   * @return Spart pointer to the created Group
   */
  GroupPtr createGroup(std::string _name);

  /**
   * @brief Find an element in the tables
   * @param _name name of the element
   * @return a pointer to the element. nullptr if not found
   */
  MultimediaPtr find(std::string _name);

  /**
   * @brief Display a Group if exists. Nothing happens if not found.
   * @param _name name of the Group to display
   * @param _s stream to display on
   */
  void displayGroup(std::string _name, std::ostream &_s);

  /**
   * @brief Display an object if exists. Nothing happens if not found.
   * @param _name name of the Multimedia to display
   * @param _s stream to display on
   */
  void displayObjet(std::string _name, std::ostream &_s);

  /**
   * @brief Display any Group or Multimedia in table with that name. Nothing
   * happens if not found.
   * @param _name name to find
   * @param _s stream to display on
   */
  void display(std::string _name, std::ostream &_s) {
    displayObjet(_name, _s);
    displayGroup(_name, _s);
  }

  /**
   * @brief Play an element by it's name. Nothing happens if not found
   * @param _name name of the element to play
   */
  void play(std::string _name);

  /**
   * @brief Delete a Multimedia if exists. It will be removed from all Group .
   * @param _name name of the Multimedia to delete
   */
  void removeObjet(std::string _name);

  /**
   * @brief Delete a Group if exists
   * @param _name name of the Group to delete
   */
  void removeGroup(std::string _name);

  /**
   * @brief Remove any Group or Multimedia matching this name
   * @param _name name to delete
   */
  void remove(std::string _name) {
    removeObjet(_name);
    removeGroup(_name);
  }

  /**
   * @brief Save content of Multimedia table to a file
   * @param fileName filename to store the content
   * @return  true if saved successfully
   */
  bool save(const std::string &fileName);

  /**
   * @brief Create a multimedia with the right class
   * @param classname name of the class to create
   * @return Smart pointer to desired class
   */
  MultimediaPtr createMultimedia(std::string const classname) const;

  /**
   * @brief Load saved content of the Multimedia table
   * @param fileName file name to read
   * @return true if loaded successfully
   */
  bool load(const std::string &fileName);

  /**
   * @brief Runs a TCP request
   * @param cnx connection incomming
   * @param request request message
   * @param response response to send
   * @return false if connection needs to be closed, true otherwise
   */
  bool processRequest(cppu::TCPConnection &cnx, const std::string &request,
                      std::string &response);
};

#endif // MANAGER_H
