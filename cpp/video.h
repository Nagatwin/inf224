﻿#ifndef VIDEO_H
#define VIDEO_H
#include "multimedia.h"

/**
 * @brief The Video class
 */
class Video : public Multimedia {
private:
  int duration = 0;

public:
  /**
   * @brief Video constructor
   */
  Video() : Multimedia() {}

  /**
   * @brief Video constructor
   * @param _name name of the Video
   * @param _path path of the Video
   * @param _duration duration of the Video
   */
  Video(std::string _name, std::string _path, int _duration)
      : Multimedia(_name, _path), duration(_duration) {}

  /**
   * @brief ~Video destructor
   */
  virtual ~Video() { std::cout << "Destructeur de video" << std::endl; }

  /**
   * @param _duration new duration of the Video
   */
  void setDuration(int _duration) { duration = _duration; }

  /**
   * @return duration of the Video
   */
  int getDuration() const { return duration; }

  /**
   * @return "Video"
   */
  virtual std::string getClass() const override { return "Video"; }

  /**
   * @brief Display the Video attributes
   * @param _s stream to display on
   */
  virtual void display(std::ostream &_s) const override;

  /**
   * @brief Play the Video
   */
  void play() const override { system(("mpv " + getPath() + " &").c_str()); }

  /**
   * @brief Write the Video to a stream (Serialize)
   * @param f stream to write on
   */
  virtual void write(std::ostream &f) const override;

  /**
   * @brief Read the video from a stream (Deserialize)
   * @param f stream to read from
   */
  virtual void read(std::istream &f) override;
};

/**
 * @brief Smart pointer to a Video
 */
typedef std::shared_ptr<Video> VideoPtr;
#endif // VIDEO_H
