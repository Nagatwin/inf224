﻿#ifndef FILM_H
#define FILM_H
#include "video.h"
/**
 * @brief The Film class
 */
class Film : public Video {
private:
  int chapterCount = 0;
  int *chaptersDuration = nullptr;

public:
  Film() : Video() {}

  /**
   * @brief Film
   * @param _name name of the Film
   * @param _path path of the Film
   * @param _chaptersDuration Durations of the capters as an int table
   * @param _chapterCount count of the chapters
   */
  Film(std::string _name, std::string _path, int *_chaptersDuration,
       int _chapterCount);

  /**
   * @brief Film constructor from another film
   * @param from Film to copy
   */
  Film(const Film &from);

  /**
   * @brief Override of operator =
   * @param from Film to copy
   * @return  Film obtained
   */
  Film &operator=(const Film &from);

  /**
   * @brief ~Film destructor
   */
  virtual ~Film() {
    // No memory leak :x
    delete[] chaptersDuration;
    std::cout << "Destructeur de film" << std::endl;
  }

  /**
   * @brief Display the Film's attributes
   * @param _s stream to display on
   */
  void display(std::ostream &_s) const override;

  /**
   * @brief Set the chapters of the Film
   * @param _chaptersDuration table of the durations of the chapters
   * @param _chapterCount count of chapters
   */
  void setChapters(int const *_chaptersDuration, int _chapterCount);

  /**
   * @brief get chapters count
   * @return the amount of chapters
   */
  int getChapterCount() const { return chapterCount; }

  /**
   * @brief Get chapters's durations
   * @return the durations of the chapters of the Film
   */
  int const *getChaptersDuration() const;

  /**
   * @return "Film"
   */
  std::string getClass() const override { return "Film"; }

  /**
   * @brief Write the film attributes to the stream (Serialize)
   * @param f stream to write on
   */
  void write(std::ostream &f) const override;

  /**
   * @brief read the attributes from the stream (Deserialize)
   * @param f stream to read from
   */
  void read(std::istream &f) override;
};

/**
 * @brief Smart pointer to a Film
 */
typedef std::shared_ptr<Film> FilmPtr;
#endif // FILM_H
