﻿//
// main.cpp
// Created on 21/10/2018
//
#include "film.h"
#include "group.h"
#include "manager.h"
#include "multimedia.h"
#include "photo.h"
#include "video.h"
#include <iostream>
#include "tcpserver.h"
#include <memory>
#include <sstream>
#include <string>
using namespace std;
using namespace cppu;

const int PORT = 3331;

int main(int argc, char *argv[]) {
  // cree le TCPServer
  shared_ptr<TCPServer> server(new TCPServer());

  // cree l'objet qui gÃ¨re les donnÃ©es
  shared_ptr<Manager> base(new Manager());

  base->load("database_serialized.txt");
  
  GroupPtr group = base->createGroup("Les bandits");
  MultimediaPtr videoptr = base->createVideo("--nameVideo--", "Heysam.mp4", 255);
  group->push_back(videoptr);

  // le serveur appelera cette mÃ©thode chaque fois qu'il y a une requÃªte
  server->setCallback(*base, &Manager::processRequest);

  // lance la boucle infinie du serveur
  cout << "Starting Server on port " << PORT << endl;
  int status = server->run(PORT);

  // en cas d'erreur
  if (status < 0) {
    cerr << "Could not start Server on port " << PORT << endl;
    return 1;
  }

  return 0;
}
