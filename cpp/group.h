﻿#ifndef GROUP_H
#define GROUP_H
#include "multimedia.h"
#include <list>
#include <memory>

/**
 * @brief A group of Multimedia items
 */
class Group : public std::list<MultimediaPtr> {
private:
  std::string name;

public:
  /**
   * @brief Group constructor
   * @param _name name of the Group
   */
  Group(std::string _name) : std::list<MultimediaPtr>(), name(_name) {}

  /**
   * @brief ~Group destructor
   */
  virtual ~Group() {
    clear();
    std::cout << "Destructeur de group" << std::endl;
  }

  /**
   * @brief Sets the name of the Group
   * @param _name new name
   */
  void setName(std::string _name) { name = _name; }

  /**
   * @brief Gets the name of the Group
   * @return name of the Group
   */
  std::string getName() const { return name; }

  /**
   * @brief Displays the attributes of the Group
   * @param _s stream to display on
   */
  void display(std::ostream &_s) const;
};

/**
 * @brief Smart pointer to a Group
 */
typedef std::shared_ptr<Group> GroupPtr;
#endif // GROUP_H
