﻿#include "manager.h"

MultimediaPtr Manager::createPhoto(std::string _name, std::string _path,
                                   float _latitude, float _longitude) {
  // Remove object if it exists
  removeObjet(_name);

  // Create photo
  Photo *p = new Photo(_name, _path, _latitude, _longitude);
  objets[_name] = MultimediaPtr(p);
  return objets[_name];
}

MultimediaPtr Manager::createVideo(std::string _name, std::string _path,
                                   int _duration) {
  // Remove object if it exists
  removeObjet(_name);

  // Create video
  Video *v = new Video(_name, _path, _duration);
  objets[_name] = MultimediaPtr(v);
  return objets[_name];
}

MultimediaPtr Manager::createFilm(std::string _name, std::string _path,
                                  int *_chaptersDuration, int _chapterCount) {
  // Remove object if it exists
  removeObjet(_name);

  // Create film
  Film *f = new Film(_name, _path, _chaptersDuration, _chapterCount);
  objets[_name] = MultimediaPtr(f);
  return objets[_name];
}

GroupPtr Manager::createGroup(std::string _name) {
  // Remove group if it exists
  removeGroup(_name);

  // Create group
  groups[_name] = GroupPtr(new Group(_name));
  return groups[_name];
}

MultimediaPtr Manager::find(std::string _name) {
  if (objets.count(_name) != 0)
    return objets[_name];
  else
    return nullptr;
}

// Display a group if found
void Manager::displayGroup(std::string _name, std::ostream &_s) {
  if (groups.count(_name) != 0)
    groups[_name]->display(_s);
}

// Display an object if found
void Manager::displayObjet(std::string _name, std::ostream &_s) {
  if (objets.count(_name) != 0)
    objets[_name]->display(_s);
}

// Play an object if found
void Manager::play(std::string _name) {
  if (objets.count(_name) != 0)
    objets[_name]->play();
}

// Remove a group
void Manager::removeGroup(std::string _name) {
  if (groups.count(_name) != 0)
    groups.erase(_name);
}

// Remove an object
void Manager::removeObjet(std::string _name) {
  // Check if it exists
  if (objets.count(_name) == 0)
    return;
  else {
    // Remove it from all groups
    for (std::map<std::string, GroupPtr>::iterator it = groups.begin();
         it != groups.end(); ++it)
      it->second->remove(objets[_name]);
  }

  // Remove object
  objets.erase(_name);
}

bool Manager::save(const std::string &fileName) {
  std::ofstream f(fileName.c_str());
  if (!f) {
    std::cerr << "Can't open file " << fileName << std::endl;
    return false;
  }

  // Save all the objetcs of the table
  for (std::map<std::string, MultimediaPtr>::iterator it = objets.begin();
       it != objets.end(); ++it)
    it->second->write(f);
  return true;
}

MultimediaPtr Manager::createMultimedia(std::string const classname) const {
  // Basically a class factory
  if (classname == "Photo")
    return MultimediaPtr(new Photo());
  else if (classname == "Video")
    return MultimediaPtr(new Video());
  else if (classname == "Film")
    return MultimediaPtr(new Film());
  else
    return MultimediaPtr(nullptr);
}

bool Manager::load(const std::string &fileName) {
  std::ifstream f(fileName);
  if (!f) {
    std::cerr << "Can't open file " << fileName << std::endl;
    return false;
  }

  std::string className;
  // Get classname to build the correct class
  while (std::getline(f, className)) {
    MultimediaPtr m = createMultimedia(className);

    // If created successfully, read it
    if (m) {
      m->read(f);

      // Remove from tables if exist
      remove(m->getName());

      // Add it to tables
      objets[m->getName()] = m;
    } else {
      return false;
    }
  }
  return true;
}

bool Manager::processRequest(cppu::TCPConnection &cnx,
                             const std::string &request,
                             std::string &response) {
  std::cerr << "\nRequest: '" << request << "'" << std::endl;

  // Split the request
  std::string req, name;
  std::stringstream string_stream;
  string_stream.str(request);
  std::getline(string_stream, req, '#');
  std::getline(string_stream, name, '#');

  // Check argument
  if (name == "") {
    // Bad argument detected

    response = "BAD_ARGUMENT: " + request;
  } else {
    // Play the request
    if (req == "play") {
      cppu::TCPLock lock(cnx);

      play(name);
      response = "OK: " + request;
    } else if (req == "search") {
      cppu::TCPLock lock(cnx, true);

      std::stringstream displayed_args;
      display(name, displayed_args);
      response = "OK: " + request + "#" + displayed_args.str();

      // Vous pouvez rajouter d'autres commandes, par exemple chercher tous
      // les objets commencant par ou contenant une sÃ©quence de caractÃ¨res, ou
      // Ã©tant d'un certain type, ou encore afficher toute la base ou dÃ©truire
      // un objet ou un groupe.
    } else {
      // Bad action requested
      response = "BAD_ACTION: " + request;
    }
  }
  std::cerr << "response: " << response << std::endl;

  return true;
}
