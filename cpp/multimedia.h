﻿#ifndef MULTIMEDIA_H
#define MULTIMEDIA_H
#include <iostream>
#include <memory>
#include <string>

/**
 * @brief Parent class of all Multimedia items
 */
class Multimedia {
private:
  std::string name = "Undefined";
  std::string path = "Undefined";

public:
  /**
   * @brief Multimedia constructor
   */
  Multimedia() {}

  /**
   * @brief Multimedia constructor
   * @param _name name of the Multimedia
   * @param _path path of the Multimedia
   */
  Multimedia(std::string _name, std::string _path) : name(_name), path(_path) {}

  /**
   * @brief ~Multimedia destructor
   */
  virtual ~Multimedia();

  /**
   * @return name of the Multimedia
   */
  std::string getName() const { return name; }

  /**
   * @return path of the Multimedia
   */
  std::string getPath() const { return path; }

  /**
   * @brief Get the class of the Multimedia
   * @return The name of the class
   */
  virtual std::string getClass() const = 0;

  /**
   * @brief Set the name of the Multimedia
   * @param _name new name to set
   */
  void setName(std::string _name) { name = _name; }

  /**
   * @brief Set the path of the Multimedia
   * @param _path new path to set
   */
  void setPath(std::string _path) { path = _path; }

  /**
   * @brief Display the attributes
   * @param _s stream to display on
   */
  virtual void display(std::ostream &_s) const;

  /**
   * @brief Play a multimedia
   */
  virtual void play() const = 0;

  /**
   * @brief Write to a stream (Serialize)
   * @param f stream to write on
   */
  virtual void write(std::ostream &f) const;

  /**
   * @brief Read from a stream (Deserialize)
   * @param f stream to read from
   */
  virtual void read(std::istream &f);
};

/**
 * @brief Pointer to a Multimedia item
 */
typedef std::shared_ptr<Multimedia> MultimediaPtr;
#endif // MULTIMEDIA_H
