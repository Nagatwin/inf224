﻿#include "multimedia.h"
Multimedia::~Multimedia() {
  // Random default destructor
  std::cerr << "Destructeur du multimedia" << std::endl;
}

void Multimedia::display(std::ostream &_s) const {
  _s << "Multimedia : name : " << name << " || path : " << path;
}

void Multimedia::write(std::ostream &f) const {
  // Write attributes
  f << getClass() << "\n" << name << '\n' << path << '\n';
}

void Multimedia::read(std::istream &f) {
  // Read attributes
  std::getline(f, name);
  std::getline(f, path);
}
