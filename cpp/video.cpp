﻿#include "video.h"

void Video::display(std::ostream &_s) const {
  _s << "Video : Duration : " << duration << " >> herited ";
  Multimedia::display(_s);
}

void Video::read(std::istream &f) {
  // Read parent attributes
  Multimedia::read(f);

  // Read attributes
  std::string s;
  std::getline(f, s);
  duration = stoi(s);
}

void Video::write(std::ostream &f) const {
  // Write parent attributes
  Multimedia::write(f);

  // Write attributes
  f << duration << '\n';
}
