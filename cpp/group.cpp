﻿#include "group.h"

void Group::display(std::ostream &_s) const {
  _s << "------ Group " << name << " :";
  for (MultimediaPtr m : *this) {
    m->display(_s);
  }
  _s << "End of group ------" << std::endl;
}
