﻿#ifndef PHOTO_H
#define PHOTO_H
#include "multimedia.h"

/**
 * @brief The Photo class
 */
class Photo : public Multimedia {
private:
  float latitude = 0;
  float longitude = 0;

public:
  /**
   * @brief Photo constructor
   */
  Photo() : Multimedia() {}

  /**
   * @brief Photo constructor
   * @param _name name of the Photo
   * @param _path path to the Photo
   * @param _latitude latitude of the Photo
   * @param _longitude longitude of the Photo
   */
  Photo(std::string _name, std::string _path, float _latitude, float _longitude)
      : Multimedia(_name, _path), latitude(_latitude), longitude(_longitude) {}

  /**
   * @brief ~Photo destructor
   */
  virtual ~Photo() { std::cout << "Destructeur de photo" << std::endl; }

  /**
   * @brief Set the latitude of the photo
   * @param _latitude new latitude
   */
  void setLatitude(float _latitude) { latitude = _latitude; }

  /**
   * @brief Set the longitude of the photo
   * @param _longitude new longitude
   */
  void setLongitude(float _longitude) { longitude = _longitude; }

  /**
   * @return Latitude of the Photo
   */
  float getLatitude() const { return latitude; }

  /**
   * @return Longitude of the Photo
   */
  float getLongitude() const { return longitude; }

  /**
   * @return "Photo"
   */
  std::string getClass() const override { return "Photo"; }

  /**
   * @brief Display the attributes of the object
   * @param _s stream to display on
   */
  void display(std::ostream &_s) const override;

  /**
   * @brief Play the Photo (display it)
   */
  void play() const override { system(("imagej " + getPath() + " &").c_str()); }

  /**
   * @brief Write the Photo to a stream (Serialize)
   * @param f stream to write on
   */
  void write(std::ostream &f) const override;

  /**
   * @brief Read the photo from a stream (Deserialize)
   * @param f stream to read from
   */
  void read(std::istream &f) override;
};

typedef std::shared_ptr<Photo> PhotoPtr;
#endif // PHOTO_H
